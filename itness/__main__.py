from optparse import OptionParser
from configparser import ConfigParser

import datetime

from itness import *

class Settings:
    club_id = None
    tzoffset = None
    interests = []
    reminders = 30
    start_date = None
    sqlite_file = None
    sms_to = None
    sms_from = None
    twilio_account = None
    twilio_token = None

    @classmethod
    def from_config(cls, config_file, options):
        settings = Settings(options)
        config = ConfigParser()
        config.read(config_file)

        settings.club_id = config.get('24hourfitness', 'club') or options.club_id
        settings.tzoffset = config.get('24hourfitness', 'tzoffset') or options.tzoffset
        interests = config.get('24hourfitness', 'interests') or options.interests
        if interests:
            settings.interests = interests.lower().split(',')
        settings.reminders = int(config.get('24hourfitness', 'reminders')) or options.reminders

        settings.sqlite_file = config.get('database', 'sqlite') or options.sqlite_file

        settings.sms_to = config.get('twilio', 'to') or options.sms_to
        settings.sms_from = config.get('twilio', 'from') or options.sms_from
        settings.twilio_account = config.get('twilio', 'account') or options.twilio_account
        settings.twilio_token = config.get('twilio', 'token') or options.twilio_token

        return settings

    def __init__(self, options):
        self.club_id = options.club_id
        self.tzoffset = options.tzoffset
        interests = options.interests
        if interests:
            self.interests = interests.split(',')
        self.reminders = options.reminders
        self.sqlite_file = options.sqlite_file
        self.start_date = options.start_date
        self.sms_to = options.sms_to
        self.sms_from = options.sms_from
        self.twilio_account = options.twilio_account
        self.twilio_token = options.twilio_token



def get_options():
    parser = OptionParser()
    parser.add_option('-t', '--test', dest='test',
                      help='Just import the data in the sample json.', action='store_true', default=False)
    parser.add_option('-c', '--config', dest='config_file',
                      help='If provided, options are filled by config file and command line arguments except "start" are ignored.', type=str, default='/etc/itness/itness.ini')

    parser.add_option('-i', '--club', dest='club_id',
                      help='The club id to query.', type=str, default='00659')
    parser.add_option('-l', '--interests', dest='interests',
                      help='The a comma separated list of class names to alert for. If blank, no notifications will be sent.', type=int, default=None)
    parser.add_option('-w', '--reminders', dest='reminders',
                      help='When to send reminders (in minutes) prior to interests class start. This will be sent as close to the reminder time as possible depending on the interval of the crontab', type=int, default=30)
    parser.add_option('-s', '--start', dest='start_date',
                      help='The start date to query. If not provided, defaults to current date.', type=str, default=datetime.now())
    parser.add_option('-z', '--tzoffet', dest='tzoffset',
                      help='The time zone offset from UTC in ±HHMM format. Defaults to CST: -0600. Does not account for Daylight Savings Time currently.', type=str, default='-0600')
    parser.add_option('-d', '--sqlite', dest='sqlite_file',
                      help='The sqlite3 file to track updates.', type=str, default='./schedules.sqlite3')

    parser.add_option('-n', '--to', dest='sms_to',
                      help='The phone number to text to.', type=str, default='+15555555555')
    parser.add_option('-f', '--from', dest='sms_from',
                      help='The phone number to text to.', type=str, default='+15555555555')
    parser.add_option('-a', '--account', dest='twilio_account',
                      help='The Twilio account to auth with.', type=str, default='')
    parser.add_option('-k', '--token', dest='twilio_token',
                      help='The Twilio account token to auth with.', type=str, default='')


    return parser.parse_args()

options, args = get_options()

if options.config_file:
    settings = Settings.from_config(options.config_file, options)
else:
    settings = Settings(options)

if options.test:
    print('Running test.')
    test_schedule_to_sqlite(settings.sqlite_file, settings.sms_to, settings.sms_from, settings.twilio_account, settings.twilio_token)
else:
    schedules = get_json(settings.club_id, settings.start_date, settings.tzoffset)
    if schedules:
        sync_schedule_to_sqlite(schedules, settings.sqlite_file, settings.interests, settings.sms_to, settings.sms_from, settings.twilio_account, settings.twilio_token)
    else:
        print('No schedules parsed.')

    print('Checking for reminders...')
    if settings.reminders:
        send_reminders(settings.sqlite_file, settings.interests, settings.reminders, settings.tzoffset, settings.sms_to, settings.sms_from, settings.twilio_account, settings.twilio_token)
