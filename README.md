# 24 Hour Fitness reminder cron script

Steps to win:
- Visit https://schedule.24hourfitness.com/_services/Calendar/getClasses?clubid={}&startdate={} 
  filling in the club id and state accordingly.
  - Club id can be found by finding your club on the website and getting the id from the url.
- Know the name of the classes you want to attend according to the api -- usually it's just one word like "BODYPUMP".
- Get a Twilio account.
- Create an itness.ini somewhere on your server ('/etc/itness/itness.ini') for example.
- Fill in all the details (explainations in Config File section below.
- Set up a crontab entry that runs this script. Preferably every 5 - 10 minutes. 
  - More than that is just mean to the 24hourfitness servers, so don't ruin for all of us by over-requesting.
   
   
## Config File
- *club*: A number that represents the club according to 24 Hour Fitness. 
- *tzoffset*: The club's timezone offset. The time zone offset from UTC in ±HHMM format. 
  This doesn't adjust for DST, so you will have to update this twice a year.
- *interests*: A comma separated list of classes you want to be notified about in case of cancelation or upcoming.
- *reminders*: The amount of time before a class in which you should reminded. This won't be exact because of the crontab alignment. 
- *sqlite*: The path to the database file that should track the classes.
- *to*: The phone number to text reminders/cancelations to. (Full number including country code is required by Twilio.)
- *from*: The Twilio phone number you can send from.
- *account*: The Twilio account number. Usually starts with "AC".
- *token*: The secret token provided by Twilio to use their API.

## Suggested install pattern
- `sudo su -`
- `git clone git@bitbucket.org:oliverseal/itness /opt/itness`
- `cd /opt/itness`
- `python3.5 -m venv ./env`
- `source env/bin/activate`
- `pip install twilio`
- `touch /var/log/itness.log`
- `mkdir /etc/itness`
- `cp itness.example.ini /etc/itness/itness.ini`
- `vim /etc/itness/itness.ini`
- `cp itness.crontab /etc/cron.d/itness`
- `vim /etc/cron.d/itness`
- `service cron restart`